## Generalist Logger
extends Node

enum LOG_TYPE {DEBUG, INFO, WARN, ERROR}

var level = LOG_TYPE.DEBUG # default
var _history = []

func set_level(_level:LOG_TYPE) -> void:
	level = _level

func log(_origin, _type:int, _message:String) -> void: # used later
	if _type < level: return # ignore
	var t = Thread.new()
	t.start(_log.bind(_origin, _type, _message), Thread.PRIORITY_LOW)
	t.wait_to_finish()

func time_convert(time_in_usec:int):
	var remainer := time_in_usec % 1000000
	var time_in_sec := int((time_in_usec - remainer) / 1000000.)
	var seconds := time_in_sec%60
	var minutes := (int(time_in_sec/60.))%60
	var hours := int((time_in_sec/60.)/60.)
	return "%02d:%02d:%02d.%06d" % [hours, minutes, seconds, remainer]

func _log(_og, _type:int, _message:String) -> void:
	var txt := PackedStringArray([])
	txt += PackedStringArray([time_convert(Time.get_ticks_usec()), " - "])
	if "name" in _og:
		txt += PackedStringArray(["[", _og.name, "]"]) # name of the origin
	else:
		txt += PackedStringArray(["[", _og.resource_name, "]"])
	txt += PackedStringArray([" ", _message]) # message
	_history.append(txt) # add to history for possible dumping
	match _type:
		LOG_TYPE.DEBUG:
			txt.insert(0, "[color=#5B8D33]")
			txt.append("[/color]")
			print_rich("".join(txt))
		LOG_TYPE.INFO:
			txt.insert(0, "[color=#9ABDDC]")
			txt.append("[/color]")
			print_rich("".join(txt))
		LOG_TYPE.WARN:
			push_warning("".join(txt.slice(2)))
			txt.insert(0, "[color=#F4C81A]")
			txt.append("[/color]")
			print_rich("".join(txt))
		LOG_TYPE.ERROR:
			printerr("".join(txt.slice(2)))
			txt.insert(0, "[color=#CB0000]")
			txt.append("[/color]")
			print_rich("".join(txt))
		_:
			print("".join(txt))
