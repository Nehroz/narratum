extends Node

var _llama_server_location : String = "D:/07_API/llama-b3205-bin-win-cuda-cu12.2.0-x64/llama-server.exe" ## Llama server location
var _model_path : String = "C:/Users/Nehroz/.cache/lm-studio/models/NousResearch/Hermes-2-Pro-Mistral-7B-GGUF/Hermes-2-Pro-Mistral-7B.Q6_K.gguf" ## Llama model location
var _pid : int = -1
var _current_runtime_args : Array[String] = []
var _check_timer : Timer = null

## Default server args
var runtime_args = {
	"model": _model_path,
	"host": NarratumConst.DEFAULT_HOST,
	"port": NarratumConst.DEFAULT_PORT,
	"threads": 4,
	"ctx-size": 2048,
	"n-gpu-layers": 10
}

func _ready() -> void:
	tree_exiting.connect(stop_server) # Stop server on exit
	_check_timer = Timer.new()
	_check_timer.wait_time = 5
	_check_timer.connect("timeout", _check)
	add_child(_check_timer)

## Starts a server with kwargs, kwargs override defaults, else defaults are used.
func start_server(kwargs={}) -> void:
	var arguments = runtime_args.duplicate()
	for key in kwargs: # Add all kwargs
		arguments[key] = kwargs[key]
	var args : Array[String] = []
	for key in arguments: # Convert args
		if key == "model": # wrap the model path in quotes
			args.append_array(["--" + key, '"'+arguments[key]+'"'])
		args.append_array(["--" + key, str(arguments[key])])
	_current_runtime_args = args
	_pid = OS.create_process(_llama_server_location, args, true)
	_check_timer.start(5)

## Stops the server
func stop_server() -> void:
	_check_timer.stop()
	OS.kill(_pid)

## Reload the server with new config
func reload(kwargs={}) -> void:
	stop_server()
	start_server(kwargs)

## Reboot the server with last used config
func reboot() -> void:
	if _current_runtime_args.size() == 0:
		push_warning("Cannot reboot server without running server first.")
	else:
		_pid = OS.create_process(_llama_server_location, _current_runtime_args, true)

## Checks if the server process is still running
func _check():
	if not OS.is_process_running(_pid):
		reboot()
	else:
		print("Server is still running. Checking in 5 seconds.")

## Called when the node exits the scene tree.
func _on_exit_tree() -> void:
	stop_server()