## Use the lcoal copy of the Lama C++ API
##
## @experimental

extends Node
class_name  LamaCppAPI

var url = NarratumConst.DEFAULT_HOST
var port = NarratumConst.DEFAULT_PORT

signal result(result:Dictionary) ## Normal response
signal stream(result:Dictionary) ## Used for streamed data # ! Not implemented
signal tokenized(result:Dictionary) ## Used for tokenized data
signal detokenized(result:Dictionary) ## Used for detokenized data

var default_args := {
	"temperature": 0.7,
	"top_k": 40,
	"top_p": 0.95,
	"repeat_penalty": 1.1,
	"seed": -1,
	"stream": false
}

# makes a request, reused code
func _request(_url:String, _method:int, _body:String, _outbound:Callable) -> void:
	var req = HTTPRequest.new()
	add_child(req)
	req.request(_url, ["Content-Type: application/json"], _method, _body)
	req.request_completed.connect(_outbound)

# handle complete response, return dict
func _response(_result, _response_code, _headers, body) -> Dictionary: # TODO add error handling
	var json := JSON.new()
	json.parse(body.get_string_from_utf8())
	var response = json.get_data()
	return response

func completion(ctx:String, kwargs={}) -> void:
	var args := default_args.duplicate()
	args["prompt"] = ctx # add prompt
	for key in kwargs: # override defaults
		args[key] = kwargs[key]
	var body = JSON.stringify(args)
	_request("http://" + url + ":" + str(port) + "/completions", HTTPClient.METHOD_POST, body, self._completion)

func _completion(_result, _response_code, _headers, body):
	var response := _response(_result, _response_code, _headers, body)
	emit_signal("result", response)

func tokenize(ctx:String) -> void:
	var body = JSON.stringify({"content": ctx})
	_request("http://" + url + ":" + str(port) + "/tokenize", HTTPClient.METHOD_POST, body, self._tokenize)

func _tokenize(_result, _response_code, _headers, body):
	var response := _response(_result, _response_code, _headers, body)
	emit_signal("tokenized", response)

func detokenize(ctx:String) -> void:
	var body = JSON.stringify({"content": ctx})
	_request("http://" + url + ":" + str(port) + "/detokenize", HTTPClient.METHOD_POST, body, self._detokenize)

func _detokenize(_result, _response_code, _headers, body):
	var response := _response(_result, _response_code, _headers, body)
	emit_signal("detokenized", response)
