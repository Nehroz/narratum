## API to interface with text-generation-webui https://github.com/oobabooga/text-generation-webui
##
## Uses localy hosted GPT.
## Currently marked as deprecated, because logits/logprobs are currently broken in the API refer https://github.com/oobabooga/text-generation-webui/issues/5954[br]
## Will be only inupgraded when the API is fixed.
## @deprecated
## @experimental
extends Node

const default_max_token = 256
var api_url := "http://127.0.0.1:5000/v1"

signal result(result:String)

func generate(ctx:String, kwargs={}) -> HTTPRequest:
	var req = HTTPRequest.new()
	add_child(req)
	kwargs["prompt"] = ctx # add prompt
	var body = JSON.stringify(kwargs)
	req.request(api_url + "/completions", ["Content-Type: application/json"], HTTPClient.METHOD_POST, body)
	req.request_completed.connect(self._generated)
	return req

func _generated(_result, _response_code, _headers, body):
	var json := JSON.new()
	json.parse(body.get_string_from_utf8())
	var response = json.get_data()
	print(response)
	emit_signal("result", response)