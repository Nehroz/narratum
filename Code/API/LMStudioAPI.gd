# Not completed, slightly usless because of the forced chat completion.
## Old LMStudio Class
##
## Will be either rewritten or removed. The Chat complition of LMStudio is a thorn.
## @deprecated
class_name LMStudioAPI
extends Node

var url = "http://localhost:1234/v1/chat/completions"

signal api_proessing

func generate(text:String, max_token:int, temp:float):
	print("send request")
	var req := HTTPRequest.new()
	add_child(req)
	var body := JSON.stringify(({
		"model": "NousResearch/Hermes-2-Pro-Mistral-7B-GGUF",
		"messages": [ 
		{ "role": "user", "content": text }
		], 
		"temperature": temp, 
		"max_tokens": max_token,
		"stream": false
	}))
	req.request(url, ["Content-Type: application/json"], HTTPClient.METHOD_POST, body)
	req.request_completed.connect(self.generated)

func generated(_result, _response_code, _headers, body):
	var json := JSON.new()
	json.parse(body.get_string_from_utf8())
	var response = json.get_data()
	print(response)
