## AI API, uses custom GPT python
##
## Uses localy hosted GPT from https://gitlab.com/Nehroz/llamaapi
## @experimental
class_name  AIAPI
extends Node

var url = "http://127.0.0.1:5000" ## URL used by hosted API.
signal api_online(status:bool) ## Emits true if online and false if not after handshake.

# TODO add top-k, top-k chestised, top-p, repeat_penalty at least.
func generate(text:String, max_token:int, temp:float, stop:PackedStringArray, chestised:PackedStringArray):
    var req = HTTPRequest.new()
    add_child(req)
    var dict := {"ctx": text, "max_token": max_token, "temp": temp}
    dict["max_token"] = max_token
    if stop.is_empty() == false:
        dict["stop"] = stop
    if chestised.is_empty() == false:
        dict["bad_seq"] = chestised
    var body = JSON.stringify(dict)
    req.request(url + "/generate", ["Content-Type: application/json"], HTTPClient.METHOD_POST, body)
    req.request_completed.connect(self.generated)

func _generated(_result, _response_code, _headers, body):
    var json := JSON.new()
    json.parse(body.get_string_from_utf8())
    var response = json.get_data()
    print(response) # TODO this needs to signal on completion and send the data back

## Tests if the API is online[br]
## Will emit api_online signal with true if online and false if not.
func handshake() -> void:
    var req = HTTPRequest.new()
    add_child(req)
    req.request(url,[],HTTPClient.METHOD_GET)
    req.request_completed.connect(self._handshake)

func _handshake(_result, _response_code, _headers, body):
    var json := JSON.new()
    json.parse(body.get_string_from_utf8())
    var response = json.get_data()
    emit_signal("api_online", response["status"])