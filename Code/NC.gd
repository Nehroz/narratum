## Constants for GDScript Narratum
##
## This is a immutable class that holds all constants used in the project.[br]
## Also includes a few utility constants.
class_name  NarratumConst

# Min and Max ints of 32 and 64 bit
const INT32_MIN := -(1 << 31) ## -2147483648
const INT64_MIN := -(1 << 63) ## -9223372036854775808
const INT32_MAX := (1 << 31) - 1 ## 2147483647
const INT64_MAX := (1 << 63) - 1 ## 9223372036854775807

const DEFAULT_HOST := "127.0.0.1"
const DEFAULT_PORT := 5234

const PFP_IMG_SIZE := Vector2i(512, 512) ## Default profile picture size

## Window Types used for Window
enum WIN_TYPE {
	LICENSE = -10,
	NEWLOAD = 0, 
	OPTIONS = 1,
	CHAR_EDITOR = 2
	}
const WIN_TYPE_STR := {
	-10: "LICENSE",
	0: "NEWLOAD",
	1: "OPTIONS",
	2: "CHAR_EDITOR"
}

## Trimming types used in the StoryContainer
enum TRIM_TYPE {
	CHUNKS = 0,
	BLOCKS = 1,
	SENTENCE = 2, # Not implimented
	WORD = 3, # Not implimented
	TOKEN = 4
	}
const TRIM_TYPE_STR := {
	0: "CHUNKS",
	1: "BLOCKS",
	2: "SENTENCE",
	3: "WORD",
	4: "TOKEN"
}

const _CHUNK_SIZE = 1024
## Static hashfunction
static func hash(path:String, expected_hash:String) -> bool:
	# Check that file exists.
	if not FileAccess.file_exists(path):
		return false
	# Start a SHA-256 context.
	var ctx = HashingContext.new()
	ctx.start(HashingContext.HASH_SHA256)
	# Open the file to hash.
	var file = FileAccess.open(path, FileAccess.READ)
	# Update the context after reading each chunk.
	while not file.eof_reached():
		ctx.update(file.get_buffer(_CHUNK_SIZE))
	# Get the computed hash.
	var res = ctx.finish()
	#printt(res.hex_encode(), Array(res)) # Print the result as hex string and array. # Only for Debug.
	if res.hex_encode() == expected_hash:
		return true
	return false