## Dice Class
##
## Allows you to roll a dice
class_name Dice
extends RefCounted

enum OPERATIONS {
	ADD = NarratumConst.INT32_MIN,
	SUB,
	MUL
}
const ZERFALL = "z"

var rng = RandomNumberGenerator.new()
var _dice_find = RegEx.new()
var _mod_find = RegEx.new()

func _init():
	_dice_find.compile("^\\d?+d\\d+z?") # grabs die
	_mod_find.compile("^\\d+\\b") # grabs digets
	print("compiled")


## Turns a dice string into a rolled dice (die).
## Example "2d6" will roll 2 6 sided dice.
## One can add advatage and disadvantage by + or - after the dice.
## Example "1d20+5" or "1d20-5" will roll 1d20 and add or sub 5 to it.
## One also can use other dice as modifiers, like "1d20+1d6" or "1d20-1d6".
## Legal dice are 2 (coin flip), any platonic solid d4, d6, d8, d10, d12, d20. You can roll illegal dice but they will not render in the DiceBox.
## To get bigger values like 100 you can modify the dice string by using an multiplier "x"
## dice ending with a z, like "1d20z" have an additional chance of falling appart, turning to dust on roll, giving them the abbility to roll 0 as well.
## Example "1d10x10+1d10z" will roll 1d20 and multiply the result by 10, then adding a d10 that can fall apart, giving you a full range of 0-100.
## [br][br]
## The reutrn will contain a [Dictionary] with the rolled dice each sub total and the seriees of die rolls as [PackedInt32Array]
## and the full total with modifiers applied.
func roll(dice_string:String) -> Dictionary:
	dice_string = dice_string.to_lower()
	var result = {
	}
	var next_op := OPERATIONS.ADD # default add to total
	var final := 0
	while dice_string.length() > 0:
		# handle modifiers, ops will default to adds even if nothing is matched.
		match dice_string[0]:
			"+":
				next_op = OPERATIONS.ADD
				dice_string = dice_string.substr(1)
				continue
			"-":
				next_op = OPERATIONS.SUB
				dice_string = dice_string.substr(1)
				continue
			"x":
				next_op = OPERATIONS.MUL
				dice_string = dice_string.substr(1)
				continue

		# try to find a dice
		var die = _dice_find.search(dice_string)
		if die:
			var split : PackedStringArray = die.get_string().split("d")
			var count := 1
			if split[0] != "":
				count = int(split[0])
			var low := 1
			if split[1].ends_with(ZERFALL):
				low = 0
			var sides := int(split[1])
			var total := 0
			var dices : PackedInt32Array = []
			for i in count:
				var rolled = rng.randi_range(low, sides)
				dices.append(rolled)
				total += rolled
			result[die.get_string()] = {"total":total, "dices":dices}
			final = Dice._apply(next_op, final, total)
			next_op = OPERATIONS.ADD # reset
			dice_string = dice_string.substr(die.get_string().length())
			continue

		# try to find a modifier
		var mod = _mod_find.search(dice_string)
		if mod:
			final = Dice._apply(next_op, final, int(mod.get_string()))
			next_op = OPERATIONS.ADD # reset
			dice_string = dice_string.substr(mod.get_string().length())

	result["total"] = final
	return result


static func _apply(op:OPERATIONS, a:int, b:int) -> int:
	match op:
		OPERATIONS.ADD:
			return a + b
		OPERATIONS.SUB:
			return a - b
		OPERATIONS.MUL:
			return a * b
		_:
			push_error("Unknown Operation: {a}".format({"a":op}))
	return 0