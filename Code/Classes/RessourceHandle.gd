extends Node

var story : StoryContainer # default to empty story

signal story_loaded()

const char_path := "user://characters/"
const trash_path := "user://trash/"

func _ready():
	empty_story()
	if !DirAccess.dir_exists_absolute("user://characters"):
		DirAccess.make_dir_absolute("user://characters")
	if !DirAccess.dir_exists_absolute(trash_path):
		DirAccess.make_dir_absolute(trash_path)
	if !DirAccess.dir_exists_absolute(trash_path + "character"):
		DirAccess.make_dir_absolute(trash_path + "character")
	

## loads an empty Story and setup.
func empty_story() -> void:
	story = StoryContainer.new()
	emit_signal("story_loaded")

## Stores data to a file
func store(_name:String) -> void:
	# dump as json
	var file = FileAccess.open("user://"+_name+".json", FileAccess.WRITE)
	var json = JSON.stringify(story) # TODO needs wrapper to read data from StoryContainer
	file.store_string(json)


## Loads data from a file
func load(_name:String) -> void:
	var file = FileAccess.open("user://"+_name+".json", FileAccess.READ)
	var json = JSON.new()
	var _err = json.parse(file.get_as_text()) # TODO Error handling
	story = json.get_data() # TODO needs wrapper to create StoryContainer
	emit_signal("story_loaded")


## store a character
func store_character(_char:Character) -> void:
	if !DirAccess.dir_exists_absolute(char_path+_char.file_orgin): # make folder
		DirAccess.make_dir_absolute(char_path+_char.file_orgin)
	else:
		SL.log(self, SL.LOG_TYPE.WARN, "Character already exists, overwriting!")
	var file = FileAccess.open(char_path+_char.file_orgin+"/char.json", FileAccess.WRITE)
	var data := _char.data
	data["name"] = _char.chr_name
	var json = JSON.stringify(data)
	file.store_string(json)
	for i in _char.chr_img_arr.size():
		if _char.chr_img_arr[i] is ImageTexture:
			var img := _char.chr_img_arr[i].get_image()
			img.save_png(char_path+_char.file_orgin+"/"+str(i)+".png")
		if _char.chr_img_arr[i] is AnimatedTexture:
			var path : String = _char.data["og_imgs_path"][i]
			DirAccess.copy_absolute(path, char_path+_char.file_orgin+"/"+str(i)+".gif")

## Returns a list of characters
func get_characters() -> Array[Character]:
	var characters : Array[Character] = []
	var dir := DirAccess.open("user://characters")
	dir.list_dir_begin()
	var folder = dir.get_next()
	while folder != "":
		if dir.current_is_dir():
			var _char := _load_char_data(folder)
			if _char is Character:
				characters.append(_char)
		folder = dir.get_next()
	dir.list_dir_end()
	return characters

## process iterating trough the characters folder.
func _load_char_data(folder:String) -> Character:
	if !FileAccess.file_exists(char_path+folder+"/char.json"): # check if at least the char.json exists
		SL.log(self, SL.LOG_TYPE.WARN, "Folder has no char.json: {a}".format({"a":folder}))
		return null
	var img_list : Array[Texture2D] = []
	var params := {"img":img_list}
	var dir := DirAccess.open(char_path+folder)
	dir.list_dir_begin()
	var file := dir.get_next()
	while file != "":
		var fpath := char_path + folder + "/" + file
		if file == "char.json":
			var f := FileAccess.open(fpath, FileAccess.READ)
			var json := JSON.new()
			var _err = json.parse(f.get_as_text())
			var data = json.get_data()
			if !data: # empty json file
				SL.log(self, SL.LOG_TYPE.WARN, "Folder has an empty json file: {a}".format({"a":file}))
				return null
			for key in data:
				params[key] = data[key]
		if file.get_extension() == "png":
			var image := Image.load_from_file(fpath)
			if image is Image:
				var new_texture := ImageTexture.create_from_image(image)
				params["img"].append(new_texture)
		if file.get_extension() == "gif":
			var gif =  GifManager.animated_texture_from_file(fpath)
			if gif is AnimatedTexture:
				params["img"].append(gif)
		file = dir.get_next()
	dir.list_dir_end()
	return Character.new(folder, params["name"], params["img"], params)

## deletes a character by moving it to trash
func delete_character(_char:Character) -> void:
	_move_folder(char_path+_char.file_orgin + "/", trash_path + "character/"+_char.file_orgin + "/")
	SL.log(self, SL.LOG_TYPE.INFO, "Character was moved to trash: " + _char.file_orgin)

## moves a folder to a new location, no deep copy; no recursion.
func _move_folder(source:String, target:String) -> void:
	if !DirAccess.dir_exists_absolute(target):
		DirAccess.make_dir_absolute(target)
	var dir := DirAccess.open(source)
	dir.list_dir_begin()
	var file = dir.get_next()
	while file != "":
		var err := DirAccess.copy_absolute(source+"/"+file, target+"/"+file)
		if err == OK and FileAccess.file_exists(target+"/"+file):
			DirAccess.remove_absolute(source+"/"+file)
		file = dir.get_next()
	dir.list_dir_end()
	DirAccess.remove_absolute(source)
	SL.log(self, SL.LOG_TYPE.INFO, "Folder {a} was moved to {b}. ".format({"a":source, "b":target}))

## create a sharable image with embedded character
func create_sharable_image(_char:Character) -> void:
	pass

## load a sharable image
func load_sharable_image(_file:String) -> Character:
	return null