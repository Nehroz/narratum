@icon("res://Graphic/Icons/layers.svg")
class_name StoryChunk
extends Object
## Story Chunk is a subcontainer to hold on to a multiple Story Blocks.
##
## As a story unfolds it may become smarter to unload past chunks of StoryBlocks.[br]
## This also will help with automatic summarization or the possibility embedings history.
## Most story generators do not manage data post the size of the models context, and
## simply forget about it. StoryChunks aims to be a quantisation to solve this.
## @experimental

var _name : String = "StoryChunk"
var _idx : int = 0 # index of this chunk
var blocks : Array[StoryBlock] = []
var total_token_size :int = 0 ## Tracked total count of tokens inside the chunk.
var block_seperator : String = "" ## If there is anything untyped like a linebreak between each block.

## Needs a index for sorting
func _init(idx:int):
	_idx = idx

## Get name
func name() -> String:
	return _name + str(_idx)

## adds a new block
func add_new_block(block:StoryBlock) -> void:
	blocks.append(block)
	total_token_size += block.size() # update size

## gets last block
func get_last_block() -> StoryBlock:
	return blocks[-1]

## get last block idx
func get_last_block_idx() -> int:
	return blocks[-1]._idx

## Will find a Story Block with idx and delete it.
func delete_block(idx:int) -> bool:
	for block in blocks:
		if block.idx == idx:
			block.free()
			SL.log(self, SL.LOG_TYPE.DEBUG, "Deleted block with index {a} in chunk {b}.".format({"a":idx, "b":_idx}))
			return true
	return false

## Returns stored tokenized data of chunk.
func get_tokens() -> PackedInt32Array:
	var t : PackedInt32Array = []
	for block in blocks:
		t.append_array(block.tokens)
	return t

## Returns whole chunk as plain text.
func get_text() -> String:
	var t : PackedStringArray = []
	for block in blocks:
		t.append(block.content)
	return block_seperator.join(t)


## Returns Chunk size
func size():
	return blocks.size()