## Spellchecking
##
## Load this as a composite below the TextEdit node.
## @deprecated

class_name SpellChecker
extends Node

## Symbols that need to be closed.
var open_symbols : PackedStringArray = [
    "(", "[", "{", "<", "«"
]
var closed_symbols : PackedStringArray = [
    ")", "]", "}", ">", "»"
]
var ambigious_symbols : PackedStringArray = [
    '"', "'", "`"
]
var punctuation_symbols : PackedStringArray = [
    ".", ",", ":", ";", "!", "?"
]

var dict : Dictionary ## contains all words
var word_size_sorted_dict : Dictionary
var common_words : PackedStringArray = []
var words_by_presufix : Dictionary = {}
var testing : bool = false

func _ready():
    dict = laod_dict()
    common_words = common_words_load()
    word_size_sorted_dict = create_size_sorted_dict(dict)
    words_by_presufix = create_presufix_dict(dict)

    #  if testing
    if testing:
        assert(levenshtein_distance("kelm", "hello") == 3, "levenshtein_distance failed!")
        var closable_test_strings = {
            "(hello) (world" : 15, "[(])": 3, "\"Hello\"" : -1,
            "[hello] world": -1, "\"a{\"}": 3 ,"(\")\"": 1,
            "'[Albert: 'Says']('x')'": -1, "'[Albert: 'Says]('x')'": 10
            }
        for k in closable_test_strings:
            assert(
                check_closables(k) == closable_test_strings[k], 
                "Closable test failed for {a}! Got {b}, expected {c}"
                .format(
                    {"a":k, "b": check_closables(k), 
                    "c": closable_test_strings[k]}
                    )
            )
        var start = Time.get_ticks_usec()
        assert(find_closest_word("helle", 10, 20, 8) == PackedStringArray(
            ["heloe", "hellen", "hello", "belle", "helve", "heller", "helly", "hell", "selle", "helge"])
            , "find_closest_word failed!")
        print(Time.get_ticks_usec() - start, "μs")


## Retunres char index where open bracket, or dialog exist that needs to be closed.
## Returns -1 if none found. Else returns idx of wrong closing symbol for brackets
# or starting idx of wrong ambigous symbol that was opened and not closed.
func check_closables(text:String) -> int:
    if text.length() >= NarratumConst.INT32_MAX:
        SL.log(self, SL.LOG_TYPE.INFO, "Text too long for indexing, passed.")
        return -1

    # contains triplet of (start, end, depth), depth is stack size
    var duos : PackedInt32Array = []
    var ignore : PackedInt32Array = []

    # Classic Stack for finding closeable pairs.
    var stack := Stack.new()
    var idx := 0
    for c in text:
        if closed_symbols.has(c):
            if stack.size() == 0:
                return idx +1
            if stack.last()[0] != open_symbols[closed_symbols.find(c)]:
                return idx +1
            else:
                # add pracket as duo
                var from : int = stack.pop()[1]
                duos += PackedInt32Array([from, idx, stack.size()])
        elif open_symbols.has(c):
            stack.push([c, idx])
        idx += 1
    if stack.size() > 0:
        return idx +1

    # walk ambigous pairs inside generated duos
    duos = PackedInt32Array([0, text.length()-1, -1]) + duos
    var temp_duos : PackedInt32Array = []
    var depth := 0
    for i in range(0, duos.size(), 3): # find deepest layer
        if duos[i+2] > depth:
            depth = duos[i+2]
    for deep in range(depth, -2, -1): # walk deepest layer
        for i in range(0, duos.size(), 3):
            if duos[i+2] == deep:
                var substring := text.substr(duos[i], duos[i+1] - duos[i] +1)
                var vidx := 0 # virtual index of currrent substring
                for c in substring:
                    if ambigious_symbols.has(c):
                        if vidx + duos[i] in ignore:
                            vidx += 1
                            continue
                        var next : int = vidx
                        while true:
                            next = substring.find(c, next+1)
                            if next + duos[i] not in ignore:break
                        if next == -1:
                            return vidx + duos[i]
                        else:
                            ignore += PackedInt32Array([vidx + duos[i], next + duos[i]])
                            temp_duos += PackedInt32Array([vidx + duos[i], next + duos[i], -1])
                    vidx += 1
    for _i in range(3):
        duos.remove_at(0)
    duos += temp_duos # add ambigous pairs

    # compare if duos overlap illegally.
    # Need to be contained within another duo or be alone in the text.
    for i in range(0, duos.size(), 3):
        var aduo := [duos[i], duos[i+1]]
        for j in range(0, duos.size(), 3):
            var bduo := [duos[j], duos[j+1]]
            if aduo == bduo: continue # skip self
            if (
                aduo[0] < bduo[0] and bduo[1] < aduo[1] # aduo is inside bduo
                or aduo[0] > bduo[0] and bduo[1] > aduo[1] # bduo is inside aduo 
                or aduo[1] < bduo[0] # bduo is post aduo
                or bduo[1] < aduo[0] # aduo is post bduo
                ):
                    continue
            return int(bduo[0]) +1
    return -1


## Simple spell checks word by word  in given tesxt.
## And returns a list tuble of idx of the word and it's word itself.
func spell_check(text:String) -> Array[Array]:
    var data := []
    var word_idx := 0
    var symbol_list : Array = punctuation_symbols + ambigious_symbols + open_symbols + closed_symbols
    for w in text.split(" "):
        # make sure no symbols are in the word
        w = w.strip_edges()
        for symbol in symbol_list:
            w = w.lstrip(symbol).rstrip(symbol)
        if w.length() > 0:
            if not dict.has(w):
                data.append([word_idx, w])
        word_idx += 1
    return data


var results : Array = []
## Find the closest words for correction, size is how many words to return
func find_closest_word(word:String, size:int, distance_min:int, threat_count:int) -> PackedStringArray:
    var start := Time.get_ticks_usec()
    var word_search : Dictionary = {}
    for w in common_words:
        word_search[w] = true
    for w in word_size_sorted_dict.get(word.length(), PackedStringArray([])):
        word_search[w] = true
    for w in words_by_presufix[word[0] + "_"]:
        word_search[w] = true
    for w in words_by_presufix["_" + word[-1]]:
        word_search[w] = true

    #split the words for threads
    var all_words : PackedStringArray = []
    for w in word_search:
        all_words.append(w)
    var word_search_split : Array[Dictionary] = []
    for i in range(threat_count):
        word_search_split.append({})
    for i in range(all_words.size()):
        word_search_split[i%threat_count][all_words[i]] = true

    print(Time.get_ticks_usec() - start, "μs and ", word_search.size(), "words")

    # Spin up threats
    var threats : Array[Thread] = []
    results.clear()
    for i in range(threat_count):
        threats.append(Thread.new())
        results.append([])
        threats[i].start(search_function.bind(word_search_split[i], word, size, distance_min, i))

    for i in range(threat_count):
        threats[i].wait_to_finish()
    # will have [[word, distance], [word, distance], ...]
    var paris : Array[Array] = []
    for subarr in results:
        for i in range(subarr[0].size()):
            paris.append([subarr[0][i], subarr[1][i]])

    # sort for top results # TODO these needs to be an actual algo, not just one element moved...
    paris.sort_custom(func(a, b): return a[1] > b[1])
    var lowest := distance_min
    var count := 0
    for e in paris:
        if e[1] < lowest:
            lowest = e[1]
            count = 1
        elif e[1] == lowest:
            count += 1
    var closest : PackedStringArray = []
    if count > size:
        for e in paris:
            if e[1] == lowest:
                closest.append(e[0])
    else:
        for i in range(size):
            closest.append(paris[i][0])
        closest.reverse()
    # reduce to size
    if closest.size() > size:
        closest.resize(size)
    return closest

func search_function(word_search:Dictionary, word:String, size:int, distance_min:int, index_target:int):
    var closest : PackedStringArray = []
    var distance : PackedInt64Array = []
    for i in range(size):
        distance.append(distance_min)
        closest.append("")

    for w in word_search:
        var d:= levenshtein_distance(word, w)
        if d < distance[0]:
            var temp : int = d
            var wtemp : String = w
            for i in range(size-1, -1, -1):
                if temp < distance[i]:
                    # mutate distance array
                    var swap := distance[i]
                    distance[i] = temp
                    temp = swap
                    # mutate closest array
                    var wswap := closest[i]
                    closest[i] = wtemp
                    wtemp = wswap
    results[index_target] += [closest, distance]
    return

## Levenshtein distance between two words
func levenshtein_distance(word1:String, word2:String) -> int:
    var distance  : Array[PackedInt32Array] = []
    for x in range(word1.length()+1):
        distance.append(PackedInt32Array([]))
        for y in range(word2.length()+1):
            distance[x].append(0)

    for x in range(word1.length()+1):
        distance[x][0] = x
    for y in range(word2.length()+1):
        distance[0][y] = y

    var a : int
    var b : int
    var c : int
    for x in range(1, word1.length()+1):
        for y in range(1, word2.length()+1):
            if word1[x-1] == word2[y-1]:
                distance[x][y] = distance[x-1][y-1]
            else:
                a = distance[x][y-1]
                b = distance[x-1][y]
                c = distance[x-1][y-1]

                if a < b and a < c:
                    distance[x][y] = a + 1
                elif b < c:
                    distance[x][y] = b + 1
                else:
                    distance[x][y] = c + 1
    return distance[word1.length()][word2.length()]


func add_distance_to_dict() -> void:
    for w in dict:
        for cm in common_words:
            dict[w][cm] = levenshtein_distance(w, cm)

func laod_dict() -> Dictionary:
    var f := FileAccess.open("res://Code/Structs/words_alpha.txt", FileAccess.READ)
    if f == null:
        return {}
    var word_dict : Dictionary = {}
    while not f.eof_reached():
        var line : String = f.get_line()
        word_dict[line] = {}
    f.close()
    return word_dict

## Not needed for comparison hence [PackedStringArray] instead of [Dictionary].
func common_words_load() -> PackedStringArray:
    var f := FileAccess.open("res://Code/Structs/The_Oxford_3000.txt", FileAccess.READ)
    if f == null:
        return PackedStringArray([])
    var words : PackedStringArray = []
    while not f.eof_reached():
        var line : String = f.get_line()
        words.append(line)
    f.close()
    return words

func create_size_sorted_dict(words:Dictionary) -> Dictionary:
    var sorted_dict : Dictionary = {}
    for w in words:
        if w.length() in sorted_dict:
            sorted_dict[w.length()].append(w)
        else:
            sorted_dict[w.length()] = PackedStringArray([w])
    return sorted_dict

func create_presufix_dict(words:Dictionary) -> Dictionary:
    var d : Dictionary = {}
    for w in words:
        if w.length() < 5: # Skip short words
            continue
        var prefix : String = w[0] + "_"
        var suffix : String = "_" + w[-1]
        if d.has(prefix): # add prefix
            d[prefix].append(w)
        else:
            d[prefix] = PackedStringArray([w])
        if d.has(suffix): # add suffix
            d[suffix].append(w)
        else:
            d[suffix] = PackedStringArray([w])
    return d
