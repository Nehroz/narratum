## Contains all data for a character.
##
##
## @experimental
extends Resource
class_name Character

var file_orgin : String = "NoName"
var chr_name : String = "Unnamed Character" ## The name of the character
var chr_img_arr : Array[Texture2D] = [] ## Contains an array of the image of the character
var data : Dictionary = { ## Contains all data for the character
	"ctx": "Unkown", # Generall appended context
	"looks" : "Unkown", # what the character looks like
	"gender" : "Unkown", # male or female or other
	"age" : "Unkown", # age in years
	"occupation" : "Unkown", # job or task
	"story" : null,  # what happend so far to the character (dynamic)
	"history" : null, # the bygone past (static)
	"motto" : [], # common motto
	"speech": [], # examples what the character says cycle list
}
var tags : PackedStringArray = [] # tags for searching

func _init(_file_name:String, _name:String, _img:Array[Texture2D], _data:Dictionary) -> void:
	file_orgin = _file_name
	chr_name = _name
	chr_img_arr = _img
	if "og_imgs_path" in _data: # only used for saving character (copy gif)
		data["og_imgs_path"] = _data["og_imgs_path"]
	for key in _data:
		if key in data:
			data[key] = _data[key]

## Creates an empty character as placeholder
static func empty(_name:String) -> Character:
	return Character.new("", _name, [], {})