@icon("res://Graphic/Icons/text.svg")
class_name StoryBlock
extends Object
## Story Block is a single block of user or AI generated text.
##
## Story Blocks contain the tokenized and purse text of human or AI source.
## Handling a quantum of the story as a block.
## @experimental

var _name : String = "Story Block"
var _idx : int = -1 ## The blocks index for sorting.
var content : String = "" ## Contains the pure string.
var tokens : PackedInt32Array = [] ## Contains the tokenized values for futher generation.
var owner : int = -1 ## Who wrote this block.

func name() -> String:
	return _name + str(_idx)

func _init(__idx:int, __text:String, __owner:int, __tokens:Array=[]) -> void:
	_idx = __idx
	content = __text
	owner = __owner
	if typeof(__tokens) == TYPE_ARRAY:
		tokens = PackedInt32Array(__tokens)
	elif typeof(__tokens) == TYPE_PACKED_INT32_ARRAY:
		tokens = __tokens
	else:
		push_error("Story Block recived a tokens not inside a Array | PackedInt32Array.")


func pack() -> Dictionary:
	return {
		"text" : content,
		"tokens" : tokens,
		"owner" : owner
	}

## Returns the token lenght of the Block.
func size() -> int: # Just a wrapper
	return tokens.size()