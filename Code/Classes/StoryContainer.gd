@icon("res://Graphic/Icons/book.svg")
class_name StoryContainer
extends Resource
## A Container to StoryChunks and it's StoryBlocks.
##
## Managing resources of a story.[br]
## Handles memory management of Story Chunks and it's allocated StoryBlocks to track
## Narratums generations. 
## @experimental

# Internal private holding all StoryChunks
var _chunks : Array[StoryChunk] = [] : 
	set(value):
		_chunks = value
	get:
		return _chunks
# holds a list of tags, for searching stories after there stored.
var _tags : PackedStringArray = []
var name : String = "Unnamed StoryCotnainer" # name of story, default

## Adds a new tag to the Array
func add_tag(v:String) -> void:
	_tags.append(v)

## Removes a existing tag from the Array
func remove_tag(v:String) -> void:
	if _tags.has(v):
		var i = _tags.find(v)
		_tags.remove_at(i)

## Returns current operating chunk, latest
func last_chunk():
	return _chunks[-1]

## Adds a new chunk to the container
func add_new_chunk(chunk:StoryChunk) -> void:
	SL.log(self, SL.LOG_TYPE.DEBUG, "Adding new chunk to StoryContainer")
	_chunks.append(chunk)

## Get last chunk
func get_last_chunk() -> StoryChunk:
	return _chunks[-1]

# returns the total amount of blocks and chunks
func _to_string():
	var total_blocks = 0
	for chunk in _chunks:
		total_blocks += chunk.blocks.size()
	return "StoryContainer hold {chunk_count} with {block_count}.".format(
		{"chunk_count": _chunks.size(),
		"block_count" : total_blocks}
	)

## Builds the context for the story, returns the Tokens.
func build_story_context(max_token:int, cutoff:NarratumConst.TRIM_TYPE) -> PackedInt32Array:
	if max_token == -1: # unlimited
		max_token = NarratumConst.INT64_MAX
	var ctx := PackedInt32Array()
	for i in range(_chunks.size()):
		if cutoff == NarratumConst.TRIM_TYPE.CHUNKS: # trim off whole chunk
			if _chunks[i].total_token_size + ctx.size() > max_token:
				break
		for j in range(_chunks[i].blocks.size()):
			var block = _chunks[i].blocks[j]
			ctx.append_array(block.tokens)
			if cutoff == NarratumConst.TRIM_TYPE.BLOCKS: # trim off whole block
				if ctx.size() > max_token:
					break
			elif cutoff == NarratumConst.TRIM_TYPE.TOKEN: # trim off token
				if ctx.size() > max_token:
					ctx = ctx.slice(0, max_token)
	return ctx
					
