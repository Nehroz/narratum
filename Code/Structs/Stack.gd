## A Stack
##
## A native stack implementation.
## Adding attional functions is possible by using the Stack class.
class_name Stack

var _arr = []

## Push an item to the stack.
func push(value) -> void:
    _arr.push_back(value)

## Pop an item from the stack.
func pop(i: int = -1):
    if i == -1:
        return _arr.pop_back()
    else:
        return _arr.pop_at(i)

## Get the size of the stack.
func size() -> int:
    return _arr.size()

## Clear the stack.
func clear() -> void:
    _arr.clear()

## Get the item at index i.
func get_item(i: int):
    return _arr[i]

## Set the item at index i.
func set_item(i: int, value: ) -> void:
    _arr[i] = value

## Get the last item of the stack without poping it.
func last():
    return _arr[-1]

## Has item in stack
func has(value) -> bool:
    return _arr.has(value)

## Convert the stack to a string
func _to_string():
    return str(_arr)