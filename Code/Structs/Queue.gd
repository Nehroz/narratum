## A Queue
##
## Stores data in a first-in, first-out (FIFO) manner
class_name Queue

var arr = []

## Add an item to the queue
func push(value) -> void:
    arr.push_front(value)

## Remove an item from the queue
func pop(i: int = -1):
    if i == -1:
        return arr.pop_back()
    else:
        return arr.pop_at(i)

## Get the size of the queue
func size() -> int:
    return arr.size()

## Clear the queue
func clear() -> void:
    arr.clear()