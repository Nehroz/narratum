# Only test higher structures
class_name Tester
extends RefCounted
const name := "Unit Tester"

func run_test() -> void:
	if OS.is_debug_build(): # Do a unit test if debug.
		SL.log(self, SL.LOG_TYPE.DEBUG, "Running unit tests...")
		check_license_intregrity()
		test_storyblock()
		test_storycontainer()
		SL.log(self, SL.LOG_TYPE.DEBUG, "Unit tests done!")
	

## Test the basic StoryChunk Class with StoryBlocks
func test_storyblock() -> void:
	var demo := ["Hello, World!", "Hello, World to you too!"]
	var demot := [[0,1,2,3,4,5], [0,1,2,3,4,5,6,7,8]]
	var o1 = StoryBlock.new(0, demo[0], 0, demot[0])
	var o2 = StoryBlock.new(1, demo[1], 1, demot[1])

	var o = StoryChunk.new(0)
	o.add_new_block(o1)
	o.add_new_block(o2)

	var demotr : PackedInt32Array = []
	for tlist in demot:
		demotr.append_array(tlist)
	assert(o.get_text() == o.block_seperator.join(demo), "Those not return all StoryBlocks text! Got {a}, expected {b}!".format({"a":o.get_text(), "b": o.block_seperator.join(demo)}))
	assert(o.get_tokens() == demotr, "Those not return all StoryBlocks Tokens!")

## Test the basic StoryContainer Class down to StoryBlocks
func test_storycontainer() -> void:
	var demo := ["Hello, World!", "Hello, World to you too!", "Thanks!"]
	var demot := [[0,1,2,3,4,5], [0,1,2,3,4,5,6,7,8], [195, 6]]
	var o1 = StoryBlock.new(0, demo[0], 0, demot[0])
	var o2 = StoryBlock.new(1, demo[1], 1, demot[1])
	var o3 = StoryBlock.new(2, demo[2], 2, demot[2])

	var oa = StoryChunk.new(0)
	var ob = StoryChunk.new(1)

	oa.add_new_block(o1)
	ob.add_new_block(o2)
	ob.add_new_block(o3)

	var o = StoryContainer.new()
	o.add_new_chunk(oa)
	o.add_new_chunk(ob)

	var expected_context : PackedInt32Array = []
	for e in demot:
		expected_context.append_array(e)
	
	assert(o.build_story_context(-1, NarratumConst.TRIM_TYPE.CHUNKS) == expected_context, "Those not return all StoryBlocks Tokens!")

## License check # ! DO NOT USE IN PRODUCTION, probably won't work and cause a error.
func check_license_intregrity() -> void:
	var hashes = [
		["LICENSE.txt", "3972dc9744f6499f0f9b2dbf76696f2ae7ad8af9b23dde66d6af86c9dfb36986"]
	]
	for o in hashes:
		assert(NarratumConst.hash(o[0], o[1]) == true, "File {a} does not match hash {b}!".format({"a":o[0], "b":o[1]}))