extends Control

@export var chat : VBoxContainer ## Contains title, send buttons, and chatbox.
@export var chatbox : VBoxContainer ## Contains chat messages
@export var characters: VBoxContainer ## Contains list of characters
const ResMsg = preload("res://Scenes/ChatWrtier/MessageBlock.tscn")
const ResCharBox = preload("res://Scenes/CharacterBox/CharacterBox.tscn")
const ResChatEnt = preload("res://Scenes/ChatWrtier/ChatEntity.tscn")

var story : StoryContainer = RH.story # Refrence copy from RessourceHandle
var chunk_size = 10 ## Number of messages in a chunk.
var chunk_counter = 0

var entites = { ## List of [Character] in chatroom and there parameters. 
	0: Character.empty("User") # default
}

# Called when the node enters the scene tree for the first time.
func _ready():
	_on_vieport_resize()
	get_viewport().connect("size_changed", _on_vieport_resize)
	SL.log(self, SL.LOG_TYPE.INFO, "ChatWritter ready!")

func _on_vieport_resize() -> void:
	# center chat, using half the screen.
	var view_port_size = get_viewport_rect().size
	chat.set_size(Vector2(view_port_size.x / 2, view_port_size.y))
	chat.position = Vector2(view_port_size.x / 2 - chat.size.x / 2, 0)

## adds a new bubble in the chat
func new_msg_bubble(text:String, sender:int) -> void:
	# add block
	var _nidx = story.last_chunk().get_last_block_idx() +1
	var ms := StoryBlock.new(_nidx, text, sender)
	if story.last_chunk().size() >= chunk_size:
		chunk_counter += 1
		story.add_new_chunk(StoryChunk.new(chunk_counter))
	story.last_chunk().add_new_block(ms)

	# add ui
	var msg = ResMsg.instantiate()
	msg.set_content(text)
	chatbox.add_child(msg)
	if sender == 0:
		msg.set_side(MessageBlock.SIDE.USER)
	else:
		msg.set_side(MessageBlock.SIDE.OTHER)
	msg.set_nametile(entites[sender].name)
	msg.set_source(story.last_chunk(), ms) # puts refrences to StoryContainer subjects
	msg.icon.texture = entites[sender].chr_img_arr[0] # TODO loop through

## Add new entity
func new_entity() -> void:
	var char_box = ResCharBox.instantiate()
	add_child(char_box)
	char_box.set_selectable(true)
	char_box.selected_char.connect(_new_character_selected)
	
func _new_character_selected(_char:Character) -> void:
	entites[entites.size()] = _char
	var chat_ent = ResChatEnt.instantiate()
	chat_ent.charname.set_text(_char.chr_name)
	chat_ent.icon.texture = _char.chr_img_arr[0]
	characters.add_child(chat_ent)