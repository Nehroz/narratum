extends Panel

@export var charname : Label
@export var status : Label
@export var icon : TextureRect
@export var kick_btn : Button
@export var ban_btn : Button

signal kick(id:int)
signal ban(id:int)

func _kick() -> void:
	emit_signal("kick", get_instance_id())

func _ban() -> void:
	emit_signal("ban", get_instance_id())