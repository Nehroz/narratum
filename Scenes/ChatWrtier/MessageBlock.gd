extends HBoxContainer
class_name MessageBlock

enum SIDE {USER, OTHER}

@export var icon : TextureRect
@export var bubble : VBoxContainer
@export var nametile : Label
@export var content : RichTextLabel
@export var timestamp : Label

var last_side = SIDE.OTHER
var source_chunk : StoryChunk = null ## stores the source ref
var source_block : MessageBlock = null ## stores the source ref


## Links the MessageBlock to a StoryChunk
func set_source(sc:StoryChunk, sb:MessageBlock) -> void:
	source_chunk = sc
	source_block = sb

## Sets position of image and name, depending on who send the message.
## User messages have the bubble on the left, other messages on the right.
func set_side(v:SIDE) -> void:
	if v == last_side:
		return
	if v == SIDE.USER:
		self.move_child(icon, 1)
		nametile.set_horizontal_alignment(HORIZONTAL_ALIGNMENT_RIGHT)
		content.set_text_direction(TEXT_DIRECTION_RTL)
		timestamp.set_horizontal_alignment(HORIZONTAL_ALIGNMENT_RIGHT)
	else:
		self.move_child(icon, 0)
		nametile.set_horizontal_alignment(HORIZONTAL_ALIGNMENT_LEFT)
		content.set_text_direction(TEXT_DIRECTION_LTR)
		timestamp.set_horizontal_alignment(HORIZONTAL_ALIGNMENT_LEFT)

## Set Name
func set_nametile(v:String) -> void:
	nametile.text = v

## Set Content
func set_content(v:String) -> void:
	content.text = v

## Set Timestamp
func set_timestamp(v:String) -> void:
	timestamp.text = v