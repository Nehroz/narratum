extends Container

const License = preload("res://Scenes/Window/License/License.tscn")
const NewLoad = preload("res://Scenes/Window/NewLoader/NewLoad.tscn")
const Options = preload("res://Scenes/Window/Options/Options.tscn")
const CharcerEditor = preload("res://Scenes/Window/CharEditor/CharEditor.tscn")
@export var parent : Panel
var child : Node

func license() -> void:
	var o = License.instantiate()
	add_child(o)
	child = o

func newload() -> void:
	var o = NewLoad.instantiate()
	o.parent = parent
	add_child(o)
	child = o

func options() -> void:
	var o = Options.instantiate()
	o.parent = parent
	add_child(o)
	child = o

func char_editor() -> void:
	var o = CharcerEditor.instantiate()
	o.parent = parent
	add_child(o)
	child = o
