extends Control

@export var e_filename : LineEdit
@export var e_name : LineEdit
@export var e_gender : LineEdit
@export var e_age : LineEdit
@export var e_ctx : TextEdit
@export var e_looks : TextEdit
@export var e_occupation : TextEdit
@export var e_story : TextEdit
@export var e_history : TextEdit
@export var e_motto : TextEdit
@export var e_speech : TextEdit

@export var i_pfp : TextureRect
@export var i_slide : Slider
@export var i_delbtn : Button
@export var i_label : Label

signal changed # soemthings was edited or added

var parent : Node
var img_arr : Array[Texture2D] # holds each texture
var orginal_image_paths : Array[String]

func _ready():
	if parent: # in case note is instanced outside of a windows; not intendet, but for testing.
		parent._set_window_size(Vector2(1200,800))
		parent.title.set_text("Character Editor")
		parent.btnmenu.get_node("Exit").set_text("Exit without saving")
		var btn = Button.new()
		btn.text = "Save and Exit"
		btn.pressed.connect(_save_char)
		btn.pressed.connect(parent._close)
		parent.btnmenu.add_child(btn)
	get_tree().get_root().connect("files_dropped", _grab_data_dropped)
	i_delbtn.hide()
	i_slide.hide()

# TODO expect window to close.


func load_char(new_char:Character) -> void:
	e_filename.text = new_char.file_orgin
	e_name.text = new_char.chr_name
	parent.title.set_text("Character Editor - " + new_char.chr_name)
	e_gender.text = new_char.data["gender"]
	e_age.text = new_char.data["age"]
	e_ctx.text = new_char.data["ctx"]
	e_looks.text = new_char.data["looks"]
	e_occupation.text = new_char.data["occupation"]
	e_story.text = new_char.data["story"]
	e_history.text = new_char.data["history"]
	e_motto.text = "-\n".join(new_char.data["motto"])
	e_speech.text = "\n".join(new_char.data["speech"])
	for i in new_char.chr_img_arr.size():
		img_arr.append(new_char.chr_img_arr[i])
	_update_images()
	if img_arr.size() > 0:
		i_pfp.texture = img_arr[0]

func _save_char() -> void:
	var _char := _backe_character()
	RH.store_character(_char)
	emit_signal("changed")

func _export_img_char() -> void:
	var _char := _backe_character()
	RH.create_sharable_image(_char)

func _backe_character() -> Character:
	var data = {}
	data["ctx"] = e_ctx.text
	data["looks"] = e_looks.text
	data["gender"] = e_gender.text
	data["age"] = e_age.text
	data["occupation"] = e_occupation.text
	data["story"] = e_story.text
	data["history"] = e_history.text
	data["motto"] = e_motto.text.split("\n-")
	data["speech"] = e_speech.text.split("\n")
	data["og_imgs_path"] = orginal_image_paths # only for saving
	var img : Array[Texture2D] = []
	if img_arr.size() > 0:
		img = img_arr
	return Character.new(e_filename.text ,e_name.text, img, data)


func _grab_data_dropped(files:PackedStringArray): # TODO check if image has additional metadata for character
	var drop_pos := get_viewport().get_mouse_position()
	# check each file
	for file in files:
		if file.get_extension() == "png" or file.get_extension() == "jpg" or file.get_extension() == "gif":
			# check if dropped on image
			var pfp_pos := i_pfp.global_position
			var pfp_size := i_pfp.get_size()
			# check if dropped on pfp
			if drop_pos.x > pfp_pos.x and drop_pos.x < pfp_pos.x+pfp_size.x and drop_pos.y > pfp_pos.y and drop_pos.y < pfp_pos.y+pfp_size.y:
				SL.log(self, SL.LOG_TYPE.INFO, "Dropped " + file + " on profile image.")
				if file.get_extension() == "gif":
					var gif =  GifManager.animated_texture_from_file(file)
					if gif is AnimatedTexture:
						img_arr.append(gif)
						orginal_image_paths.append(file)
						SL.log(self, SL.LOG_TYPE.INFO, "Added a gif.")
				else:
					var image := Image.load_from_file(file)
					if image is Image:
						image.resize(NarratumConst.PFP_IMG_SIZE.x, NarratumConst.PFP_IMG_SIZE.y, Image.INTERPOLATE_NEAREST)
					var new_texture = ImageTexture.create_from_image(image)
					if new_texture is ImageTexture:
						img_arr.append(new_texture)
						orginal_image_paths.append(file)
						SL.log(self, SL.LOG_TYPE.INFO, "Added a static image.")
				if img_arr.size() > 0:
					_update_images()
					i_pfp.texture = img_arr[-1]
					i_slide.value = img_arr.size()

func _update_images() -> void:
	if img_arr.size() > 0:
		i_delbtn.disabled = false
		i_label.hide()
		i_slide.show()
		i_delbtn.show()
		if img_arr.size() > 1:
			i_slide.editable = true
			i_slide.max_value = img_arr.size()
		else:
			i_slide.editable = false
			i_slide.value = 1
			i_slide.max_value = 1
	else:
		i_pfp.texture = PlaceholderTexture2D.new()
		i_pfp.texture.set_size(Vector2(512,512))
		i_delbtn.disabled = true # just making sure button can't be pressed when invisble and still selected by hitting enter.
		i_label.show()
		i_slide.hide()
		i_delbtn.hide()
			
# updates on slide
func _change_image(val:float) -> void:
	i_pfp.texture = img_arr[int(val)-1]

# delete image
func _delete_image() -> void:
	SL.log(self, SL.LOG_TYPE.INFO, "Delete image")
	img_arr.remove_at(int(i_slide.value)-1)
	_update_images()
	if img_arr.size() > 1:
		i_pfp.texture = img_arr[int(i_slide.value)-1]
	elif img_arr.size() == 1:
		i_pfp.texture = img_arr[0]