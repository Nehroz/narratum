extends Panel

@export var container : Container
@export var title : Label
@export var anim : AnimationPlayer
@export var btnmenu : BoxContainer

func _ready():
	get_tree().get_root().size_changed.connect(_move_into_screen) # ensure window stays inside.
	anim.play("create")

func load(window_type:NarratumConst.WIN_TYPE) -> void:
	set_modulate(Color(1, 1, 1, 0)) # Makes window invisible before animation
	match window_type:
		NarratumConst.WIN_TYPE.LICENSE:
			title.set_text("License Notice")
			container.license()
			await tree_entered # wait until it can access the screen_tree
			_center()
		NarratumConst.WIN_TYPE.NEWLOAD:
			title.set_text("New Story")
			container.newload()
		NarratumConst.WIN_TYPE.OPTIONS:
			title.set_text("Options")
			container.options()
		NarratumConst.WIN_TYPE.CHAR_EDITOR:
			title.set_text("Character Editor")
			container.char_editor()
	SL.log(self, SL.LOG_TYPE.INFO, "A window of type " + NarratumConst.WIN_TYPE_STR[window_type] + " has been loaded.")

func _close() -> void:
	anim.play("pop")
	await anim.animation_finished
	queue_free()

func _set_window_size(win_size:Vector2) -> void:
	_set_size(win_size)
	# update pivot offset
	pivot_offset = win_size / 2
	_center()


var _drag = false
func _drag_window(event:InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.is_pressed():
			_drag = true
			get_parent().move_child(self, -1) # also puts window on top
		else:
			_drag = false
			_move_into_screen()
	elif event is InputEventMouseMotion:
		if _drag:
			position += event.relative

func _move_into_screen() -> void:
	# move back if top left corner is outside screen
	if !get_viewport_rect().has_point(position):
		if position.x < 0:
			position.x = 0
		if position.y < 0:
			position.y = 0
	# move back if bottom right corner is outside screen
	if !get_viewport_rect().has_point(position + size):
		if position.x + size.x > get_viewport_rect().size.x:
			position.x = get_viewport_rect().size.x - size.x
		if position.y + size.y > get_viewport_rect().size.y:
			position.y = get_viewport_rect().size.y - size.y

# ! works fine if called after tree_entered signal, fails before. 
# ! awaiting tree_entered casuses window that are generated faster in more idle frames to be at the wrong position.
func _center() -> void:
	var win_mid = get_size() /2
	position = get_viewport_rect().size / 2 - win_mid
