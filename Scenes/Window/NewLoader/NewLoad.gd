extends HBoxContainer

var parent : Node


func _ready():
	parent._set_window_size(Vector2(200,120))

func _load():
	# TODO add FileDialog to load files, as soon as they can be stored.
	parent._close()

func _new():
	RH.empty_story()
	parent._close()