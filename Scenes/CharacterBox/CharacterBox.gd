extends VBoxContainer

@export var characacters : GridContainer

var res_win = preload("res://Scenes/Window/Window.tscn") # load window
var res_char_element = preload("res://Scenes/CharacterBox/CharElement.tscn")

signal selected_char(_char:Character)

var chars : Array[Character] = []
var char_arr : Array[Panel] = []
var selection_enabled : bool = false # if the box is open to select a character

func _ready():
	_init_character()

# selctable
func set_selectable(_selectable:bool) -> void:
	selection_enabled = _selectable
	for n in char_arr:
		n.set_selectable(_selectable)

# new character
func _new_character() -> void:
	var win = res_win.instantiate()
	win.load(NarratumConst.WIN_TYPE.CHAR_EDITOR)
	get_tree().get_root().add_child(win)
	win.container.child.changed.connect(_init_character) # update on save

# load up characters
func _init_character() -> void:
	chars.clear() # remove old
	for n in char_arr: 
		n.queue_free()
	char_arr.clear()
	chars = RH.get_characters()
	for c in chars:
		var o = res_char_element.instantiate()
		o.name = c.file_orgin
		o.get_node("Name").text = c.chr_name
		if c.chr_img_arr.size() > 0:
			o.get_node("Image").texture = c.chr_img_arr[0]
		characacters.add_child(o)
		char_arr.append(o)
		#o.selected.connect(_select)
		o.edit.connect(_edit)
		o.delet.connect(_delete_char)
		o.select.connect(_select_char)
		o.set_selectable(selection_enabled)
	SL.log(self, SL.LOG_TYPE.INFO, "Character List updated!")

## Edit character
func _edit (id:int) -> void:
	var win = res_win.instantiate()
	win.load(NarratumConst.WIN_TYPE.CHAR_EDITOR)
	get_tree().get_root().add_child(win)
	for i in char_arr.size():
		if char_arr[i].get_instance_id() == id:
			await get_tree().process_frame
			win.container.child.load_char(chars[i])
			win.container.child.changed.connect(_init_character) # update on save
			char_arr[i].set_editable(false)
			char_arr[i].set_deletable(false)
			SL.log(self, SL.LOG_TYPE.INFO, "Edit character: " + chars[i].file_orgin)
			win.tree_exited.connect(_edit_completed.bind(id))

func _edit_completed(id:int) -> void:
	for i in char_arr.size():
		if char_arr[i].get_instance_id() == id:
			char_arr[i].set_editable(true)
			char_arr[i].set_deletable(true)

func _delete_char(id:int) -> void:
	for i in char_arr.size():
		if char_arr[i].get_instance_id() == id:
			RH.delete_character(chars[i])
			chars.remove_at(i)
			char_arr[i].queue_free()

func _select_char(id:int) -> void:
	for i in char_arr.size():
		if char_arr[i].get_instance_id() == id:
			selected_char.emit(chars[i])
			_exit()

# exits editor
func _exit():
	queue_free()

func _open_folder() -> void:
	# ! Not working for some reason.
	var path := ""
	match OS.get_name():
		"Windows":
			path = "%APPDATA%\\Godot\\app_userdata\\Narratum"
		"X11":
			path = "~/.local/share/godot/app_userdata/Narratum"
		"OSX":
			path = "~/Library/Application Support/Godot/app_userdata/Narratum"
		_:
			SL.log(self, SL.LOG_TYPE.WARN, "Unknown OS")
			return
	print(path)
	OS.execute("start", [path])
