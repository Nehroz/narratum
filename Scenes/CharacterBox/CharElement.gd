extends Panel

signal select(id:int)
signal edit(id:int)
signal delet(id:int)
signal export(id:int)

@export var selectbtn : Button
@export var edittbtn : Button
@export var deletbtn : Button

func set_selectable(_value:bool) -> void:
	selectbtn.set_disabled(!_value)

func set_editable(_value:bool) -> void:
	edittbtn.set_disabled(!_value)

func set_deletable(_value:bool) -> void:
	deletbtn.set_disabled(!_value)

func _select() -> void:
	emit_signal("select", get_instance_id ())

func _edite() -> void:
	emit_signal("edit", get_instance_id ())

func _delete() -> void:
	emit_signal("delet", get_instance_id ())

func _export() -> void:
	emit_signal("export", get_instance_id ())
