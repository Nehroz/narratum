extends Control

var story : StoryContainer = RH.story # Refrence copy from RessourceHandle
var api = LamaCppAPI.new()

@export var editor : TextEdit
@export var author_node : TextEdit
@export var memory : TextEdit
@export var menu : VBoxContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	var res = load("uid://d347k05uuqrah") # load window
	var win = res.instantiate()
	win.load(NarratumConst.WIN_TYPE.NEWLOAD)
	get_tree().get_root().add_child(win)
	add_child(api)
	api.result.connect(self._gen_compelte)
	SL.log(self, SL.LOG_TYPE.INFO, "NovelWriter ready!")

# generates next chunk
func _gen() -> void:
	print("gen")
	var text = editor.text
	var schema := {
		"type": "string",
		"maxLenght": 512 # DEV # just for testing. Not ideal.
	} 
	var kwargs := {"json_schema": schema}
	api.completion(text, kwargs)

func _gen_compelte(result:Dictionary) -> void:
	if result.has("content"):
		result.erase("prompt")
		print(result)
		editor.set_text(editor.text + result["content"])
	else:
		print(result)

func _open_menu() -> void:
	if menu.is_visible(): menu.hide()
	else: menu.show()

func _input(event):
	if event is InputEventKey:
		# use enter as generation key if editor is slected.
		if editor.has_focus():
			if event.is_action_pressed("genreate") && !event.is_action_pressed("nextline"):
				_gen()
				editor.accept_event() # stop event from propagating
