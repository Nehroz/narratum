extends Control

const license_popup_time : float = 0.2
const nw_path : StringName = "res://Scenes/NovelWriter/NovelWriter.tscn"
const cw_path : StringName = "res://Scenes/ChatWrtier/ChatWritter.tscn"
const char_box : StringName = "res://Scenes/CharacterBox/CharacterBox.tscn"
var res = preload("uid://d347k05uuqrah") # load window

@export var anim : AnimationPlayer
@export var bg : Sprite2D

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().get_root().size_changed.connect(_on_resize)

	if OS.is_debug_build():
		SL.set_level(SL.LOG_TYPE.DEBUG)
		SL.log(self, SL.LOG_TYPE.DEBUG, "Debug mode is on!")
	else:
		SL.set_level(SL.LOG_TYPE.INFO)
	var o = Tester.new()
	o.run_test()
	anim.play("MenuAnimations/slide_in")
	await get_tree().create_timer(license_popup_time).timeout
	_pop_license()

	#LlamaCppWatchdog.start_server()
	#var c = LamaCppAPI.new()
	#add_child(c)
	#c.completion("Hello, I'am ")

func _noveleditor():
	anim.play("MenuAnimations/slide_out")
	var nw_resource = load(nw_path)
	var nw = nw_resource.instantiate()
	add_child(nw)

func _chateditor():
	anim.play("MenuAnimations/slide_out")
	var cw_resource = load(cw_path)
	var cw = cw_resource.instantiate()
	add_child(cw)

func _charbox():
	anim.play("MenuAnimations/slide_out")
	var char_resource = load(char_box)
	var cb = char_resource.instantiate()
	add_child(cb)

func _press_options():
	var win = res.instantiate()
	win.load(NarratumConst.WIN_TYPE.OPTIONS)
	get_tree().get_root().add_child(win)

func _pop_license() -> void: # Only pop once
	if !FileAccess.file_exists("user://first_time_notice.txt"):
		var firsttimenotification = res.instantiate()
		firsttimenotification.load(NarratumConst.WIN_TYPE.LICENSE)
		get_tree().get_root().add_child(firsttimenotification)
		var f := FileAccess.open("user://first_time_notice.txt", FileAccess.WRITE)
		var time = Time.get_datetime_dict_from_system() # store time, in case a new message should be pushed in the future; maybe, probably not.
		f.store_string("%02d:%02d:%02d - %02d.%02d.%04d" % [time.hour, time.minute, time.second, time.day, time.month, time.year])


# When resized
func _on_resize():
	var window_size : Vector2 = get_viewport().size
	var img_size : Vector2 = bg.texture.get_size()
	var ratio : Vector2 = window_size / img_size
	bg.set_scale(ratio)