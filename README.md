# Narratum

**WARNING!** This is still very early in development and not usable yet. 

Narratum is indented as a project that brings GPT based story generation and a SandboxLM experience to local systems. The likes of NovelAI, DungeonAI, StoryLab, Character.ai or whatever else is out there. The far ahead future 3rd story type besides "Novel" and "Chat"-like will be to populate a 2D abstraction, a world space, with LM based NPCs to improve story generations locality, and possibly further gamification of interactive story generation.

## Liecense
This project is licensed under the GNU General Public License v3.0. See the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)

English Word list: https://github.com/dwyl/english-words